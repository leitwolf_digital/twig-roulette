# Drupal Twig Roulette
This is a small demo to illustrate how twig loads different templates using page_manager and/or views (not sure where the bug is exactly yet) when using multiple theme suggestions for a node view mode in different contexts. Views page, views block etc.

Twig template files are located in 
`web/themes/custom/test/templates`

Database dump with demo data for a quickstart is here:
[twigroulette.sql.gz](twigroulette.sql.gz)`

#Installation (Quickmanual)
- Clone repo
- composer install 
- Install Drupal
- Import config or import above mentioned db dump

Views pages:   
`/article-list-promoted`    
`/article-list`

Page manager page:  
`/article-page`

#Findings
- Whatever page (from above) you visit first after a cache clear, those rendered twig templates will get rendered on other views even if they have their own templates.`

- E.g. if you visit the `/article-list` page first (after cache clear), also the same twig template (the one rendered by the view `web/themes/custom/test/templates/node--view--article-list-promoted--page-1.html.twig) will be selected for the other pages too, even if they have other template suggestions.
